const opml = require("opml-generator");

const csv = require("csv-parser");
const fs = require("fs");
const results = [];

const YOUR_CSV_FILENAME = "";

// if you are closing an instance, put its domain here and uncomment
// the leaving domain code a few lines down to nix feeds from
// the instance you're shutting down (since they'll be gone)
const LEAVING_DOMAIN = "";

const header = {
    title: "Thunderbird OPML Export - Fedi",
    dateCreated: new Date(),
};

fs.createReadStream(`${YOUR_CSV_FILENAME}`)
    .pipe(csv())
    .on("data", (data) => results.push(data))
    .on("end", () => {
        const forOPML = [{ title: "Mastoexport", _children: [] }];
        results.forEach((follow) => {
            const wf = follow["Account address"];
            const nick = wf.split("@")[0];
            const domain = wf.split("@")[1];
            // if (domain === LEAVING_DOMAIN) {
            //     // get rid of the accounts from the server we're shutting down
            //     return;
            // }
            let rss = `https://${domain}/@${nick}.rss`;
            let url = `https://${domain}/@${nick}`;
            if (domain.indexOf("pixelfed") >= 0) {
                rss = `https://${domain}/users/${nick}.atom`;
                url = rss;
            }

            forOPML[0]["_children"].push({
                type: "rss",
                title: wf,
                text: wf,
                version: "RSS",
                "fz:quickMode": "false",
                xmlUrl: rss,
                htmlUrl: url,
            });
        });

        const toOutput = opml(header, forOPML);

        fs.writeFile(
            "importThisToThunderbird.opml",
            toOutput,
            function (err, data) {
                if (err) {
                    return console.log(err);
                }
                console.log("exported to ./importThisToThunderbird.opml");
            }
        );
    });
