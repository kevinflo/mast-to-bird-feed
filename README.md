This allows you to turn the raw CSV exported from a mastodon instance follow list into an OPML file which can be imported into mozilla thunderbird in order to follow fediverse accounts via RSS/atom

- download the `Follows` csv from `${YOUR_INSTANCE_DOMAIN}/settings/export`
- move it into the root directory of this
- edit `mastToBirdFeed.js` by adding your filename as `YOUR_CSV_FILENAME`
- if you're shutting down an instance, also add its domain as `LEAVING_DOMAIN` and uncomment out the related lines to skip following feeds from your soon-to-be-gone instance
- `npm install`
- `npm run do-the-thing`
- open the resulting opml file in a text editor and add `xmlns:fz="urn:forumzilla:"` inside of the `<opml` tag (right before head) so the full tag reads `<opml version="2.0" xmlns:fz="urn:forumzilla:">` (I don't know why, thunderbird won't accept it without this
	(the rest of these instructions are how I did it on MacOS. I'm pretty sure it's the same on windows/linux)
- open thunderbird and go `file > new > Feed account` and enter an account name
- right click on the new feed account in the left nav and click subscribe
- click import, and select the `importThisToThunderbird.opml` file this utility generated
- right click the `Mastoexport` directory and click get messages
- if a ! icon shows up it means some of the feeds you followed don't exist anymore. Right click the `Mastoexport` directory and click subscribe. Scroll through the list to fix or clear out the dead ones. Note that some fediverse implementations need special treatment so maybe you can still find the feed